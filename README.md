## Authors

- Adrienne Louise TCHAMGOUE KAMENI
- Hany SHAMLOUL

## Choix des technologies

- MySQL : Base de données relationnelle

- TypeScript : Langage de programmation typé et sémi compilé. Il nous a permis d'implémenter de façon générique les CRUD à l'aide les dao, interfaces et classes

## Architecture du projet

- build : Repertoire généré automatiquement lors de la compilation du projet (c'est l'exécutable du projet)
- src/app : Contient tous les modules du projet
- src/app/pokemon : contient tout ce qui est en lien avec l'entité pokemon à savoir modèle, controlleur et routes
- src/app/users : contient tout ce qui est en lien avec l'entité user à savoir modèle, controlleur et routes
- src/app/trade : contient tout ce qui est en lien avec l'entité trade à savoir modèle, controlleur et routes

-src/app/public : Contient tous les utilitaires du projet : énumérations, interfaces, dao
-.env : configuration de la base de données

## Installer et lancer le projet
- npm install
- npm install -g typescript  : installer typescript de façon globale
- npm install nodemon --save : installer nodemon
- npm start : lancer le projet

- docker-compose up : lancer le projet avec docker
- docker-compose down : arrêter le projet avec docker
- docker-compose build : compiler le projet avec docker


## Scripts npm et leur utilité
 npm start : lancer le projet