/*enum RightAccess {
    USER, ADMIN
}*/
enum RightAccess {
    'users:create', 'users:read', 'users:update:all', 'users:delete:all', 'pokemons:create:all', 'pokemons:read', 'pokemons:update:all', 'pokemons:delete:all', 'trade:create:all', 'trade:read', 'trade:update:all', 'logs:read'
}