import { NextFunction, Request, Response, Router } from "express"; 
import { Schema, ValidateSchema } from "../../midleware/validations";
import passport from "passport";
import TradeController from "./controllers/trades_controller";
const tradeRouter = Router();
var tradeController = new TradeController();

function ensureAuthenticated(req: Request, res: Response, next: NextFunction) {
  console.log(req.user);
  if (req.user) {
    return next();
  }
  res.status(404).send({ message: "you must login!" });
}

tradeRouter.post("/trade", tradeController.createTrade);
tradeRouter.get("/users/:userId/trade", tradeController.getAllTradeByUser);
tradeRouter.get("/users/:userId/trade/:tradeId", tradeController.getTradeById); 
tradeRouter.patch("/trade/:tradeId", tradeController.updateTrade); 
tradeRouter.put("/trade/:tradeId", tradeController.updateTrade);
tradeRouter.get("/trade", tradeController.getAllTrade);

export default tradeRouter;