import { Pokemons } from "../../pokemon/model/pokemon_model";

interface ITrade {
  id: number;
  receiverId: number;
  giverId: number;
  tradStatus: TradeStatus;
  createdAt: string;
  updatedAt: string; 
  tradItem: Pokemons[];
}
