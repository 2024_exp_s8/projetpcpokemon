"use strict";
/*enum RightAccess {
    USER, ADMIN
}*/
var RightAccess;
(function (RightAccess) {
    RightAccess[RightAccess["users:create"] = 0] = "users:create";
    RightAccess[RightAccess["users:read"] = 1] = "users:read";
    RightAccess[RightAccess["users:update:all"] = 2] = "users:update:all";
    RightAccess[RightAccess["users:delete:all"] = 3] = "users:delete:all";
    RightAccess[RightAccess["pokemons:create:all"] = 4] = "pokemons:create:all";
    RightAccess[RightAccess["pokemons:read"] = 5] = "pokemons:read";
    RightAccess[RightAccess["pokemons:update:all"] = 6] = "pokemons:update:all";
    RightAccess[RightAccess["pokemons:delete:all"] = 7] = "pokemons:delete:all";
    RightAccess[RightAccess["trade:create:all"] = 8] = "trade:create:all";
    RightAccess[RightAccess["trade:read"] = 9] = "trade:read";
    RightAccess[RightAccess["trade:update:all"] = 10] = "trade:update:all";
    RightAccess[RightAccess["logs:read"] = 11] = "logs:read";
})(RightAccess || (RightAccess = {}));
//# sourceMappingURL=right.js.map