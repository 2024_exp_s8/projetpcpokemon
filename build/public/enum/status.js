"use strict";
var TradeStatus;
(function (TradeStatus) {
    TradeStatus[TradeStatus["PROPOSED"] = 0] = "PROPOSED";
    TradeStatus[TradeStatus["ACCEPTED"] = 1] = "ACCEPTED";
    TradeStatus[TradeStatus["CANCELED"] = 2] = "CANCELED";
})(TradeStatus || (TradeStatus = {}));
//# sourceMappingURL=status.js.map