"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const trades_controller_1 = __importDefault(require("./controllers/trades_controller"));
const tradeRouter = (0, express_1.Router)();
var tradeController = new trades_controller_1.default();
function ensureAuthenticated(req, res, next) {
    console.log(req.user);
    if (req.user) {
        return next();
    }
    res.status(404).send({ message: "you must login!" });
}
tradeRouter.post("/trade", tradeController.createTrade);
tradeRouter.get("/users/:userId/trade", tradeController.getAllTradeByUser);
tradeRouter.get("/users/:userId/trade/:tradeId", tradeController.getTradeById);
tradeRouter.patch("/trade/:tradeId", tradeController.updateTrade);
tradeRouter.put("/trade/:tradeId", tradeController.updateTrade);
tradeRouter.get("/trade", tradeController.getAllTrade);
exports.default = tradeRouter;
//# sourceMappingURL=trade_route.js.map