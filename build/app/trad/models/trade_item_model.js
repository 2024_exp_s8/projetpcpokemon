"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TradeItem {
    constructor(giverId, pokemonGiverId, pokemonReceiverId, creationDate) {
        (this.giverId = giverId),
            (this.pokemonGiverId = pokemonGiverId),
            (this.pokemonReceiverId = pokemonReceiverId),
            (this.creationDate = creationDate);
    }
}
exports.default = TradeItem;
//# sourceMappingURL=trade_item_model.js.map