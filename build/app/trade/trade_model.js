"use strict";
var __esDecorate = (this && this.__esDecorate) || function (ctor, descriptorIn, decorators, contextIn, initializers, extraInitializers) {
    function accept(f) { if (f !== void 0 && typeof f !== "function") throw new TypeError("Function expected"); return f; }
    var kind = contextIn.kind, key = kind === "getter" ? "get" : kind === "setter" ? "set" : "value";
    var target = !descriptorIn && ctor ? contextIn["static"] ? ctor : ctor.prototype : null;
    var descriptor = descriptorIn || (target ? Object.getOwnPropertyDescriptor(target, contextIn.name) : {});
    var _, done = false;
    for (var i = decorators.length - 1; i >= 0; i--) {
        var context = {};
        for (var p in contextIn) context[p] = p === "access" ? {} : contextIn[p];
        for (var p in contextIn.access) context.access[p] = contextIn.access[p];
        context.addInitializer = function (f) { if (done) throw new TypeError("Cannot add initializers after decoration has completed"); extraInitializers.push(accept(f || null)); };
        var result = (0, decorators[i])(kind === "accessor" ? { get: descriptor.get, set: descriptor.set } : descriptor[key], context);
        if (kind === "accessor") {
            if (result === void 0) continue;
            if (result === null || typeof result !== "object") throw new TypeError("Object expected");
            if (_ = accept(result.get)) descriptor.get = _;
            if (_ = accept(result.set)) descriptor.set = _;
            if (_ = accept(result.init)) initializers.push(_);
        }
        else if (_ = accept(result)) {
            if (kind === "field") initializers.push(_);
            else descriptor[key] = _;
        }
    }
    if (target) Object.defineProperty(target, contextIn.name, descriptor);
    done = true;
};
var __runInitializers = (this && this.__runInitializers) || function (thisArg, initializers, value) {
    var useValue = arguments.length > 2;
    for (var i = 0; i < initializers.length; i++) {
        value = useValue ? initializers[i].call(thisArg, value) : initializers[i].call(thisArg);
    }
    return useValue ? value : void 0;
};
var __setFunctionName = (this && this.__setFunctionName) || function (f, name, prefix) {
    if (typeof name === "symbol") name = name.description ? "[".concat(name.description, "]") : "";
    return Object.defineProperty(f, "name", { configurable: true, value: prefix ? "".concat(prefix, " ", name) : name });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Trade = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const user_model_1 = __importDefault(require("../users/models/user.model"));
exports.Trade = (() => {
    let _classDecorators = [(0, sequelize_typescript_1.Table)({
            tableName: "trade",
        })];
    let _classDescriptor;
    let _classExtraInitializers = [];
    let _classThis;
    let _instanceExtraInitializers = [];
    let _receiverId_decorators;
    let _receiverId_initializers = [];
    let _giverId_decorators;
    let _giverId_initializers = [];
    let _user_decorators;
    let _user_initializers = [];
    let _tradeStatus_decorators;
    let _tradeStatus_initializers = [];
    var Trade = _classThis = class extends sequelize_typescript_1.Model {
        constructor() {
            super(...arguments);
            this.receiverId = (__runInitializers(this, _instanceExtraInitializers), __runInitializers(this, _receiverId_initializers, void 0));
            this.giverId = __runInitializers(this, _giverId_initializers, void 0);
            this.user = __runInitializers(this, _user_initializers, void 0);
            this.tradeStatus = __runInitializers(this, _tradeStatus_initializers, void 0);
        }
    };
    __setFunctionName(_classThis, "Trade");
    (() => {
        _receiverId_decorators = [(0, sequelize_typescript_1.ForeignKey)(() => user_model_1.default), (0, sequelize_typescript_1.Column)({ type: sequelize_typescript_1.DataType.INTEGER, allowNull: false })];
        _giverId_decorators = [(0, sequelize_typescript_1.ForeignKey)(() => user_model_1.default), (0, sequelize_typescript_1.Column)({ type: sequelize_typescript_1.DataType.INTEGER, allowNull: false })];
        _user_decorators = [(0, sequelize_typescript_1.BelongsTo)(() => user_model_1.default)];
        _tradeStatus_decorators = [(0, sequelize_typescript_1.Column)({ type: sequelize_typescript_1.DataType.STRING, allowNull: true })];
        __esDecorate(null, null, _receiverId_decorators, { kind: "field", name: "receiverId", static: false, private: false, access: { has: obj => "receiverId" in obj, get: obj => obj.receiverId, set: (obj, value) => { obj.receiverId = value; } } }, _receiverId_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _giverId_decorators, { kind: "field", name: "giverId", static: false, private: false, access: { has: obj => "giverId" in obj, get: obj => obj.giverId, set: (obj, value) => { obj.giverId = value; } } }, _giverId_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _user_decorators, { kind: "field", name: "user", static: false, private: false, access: { has: obj => "user" in obj, get: obj => obj.user, set: (obj, value) => { obj.user = value; } } }, _user_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _tradeStatus_decorators, { kind: "field", name: "tradeStatus", static: false, private: false, access: { has: obj => "tradeStatus" in obj, get: obj => obj.tradeStatus, set: (obj, value) => { obj.tradeStatus = value; } } }, _tradeStatus_initializers, _instanceExtraInitializers);
        __esDecorate(null, _classDescriptor = { value: _classThis }, _classDecorators, { kind: "class", name: _classThis.name }, null, _classExtraInitializers);
        Trade = _classThis = _classDescriptor.value;
        __runInitializers(_classThis, _classExtraInitializers);
    })();
    return Trade = _classThis;
})();
//# sourceMappingURL=trade_model.js.map